# Regular FHG
def f(growth,num):
	if(growth==0):
		return(num+1)
	x=num
	for _ in range(num):
		x=f(growth-1,x)
	return(x)

# f_ω(x)
def fω(x):
	return(f(x,x))

# f_(ω+n)(x)
# may not actually work.
def fωcont(growth,x):
	if(growth==0):
		return(fω(x))
	num=x
	for _ in range(growth):
		num=fωcont(growth-1,num)
	return(num)